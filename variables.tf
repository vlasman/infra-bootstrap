# === General ===

variable "kubernetes_custom_resources_enabled" {
  description = "Disable Kubernetes custom resources to prevent failing an initial plan when Kubernetes and/or its Custom Resource Definitions are not yet available"
  type        = bool
  default     = true
}

variable "base_name" {
  description = "A name prefix for all the created resources."
  type        = string
  default     = ""
  validation {
    condition     = var.base_name == "" || length(var.base_name) < 24
    error_message = "Too long: a maximum of 24 characters allowed."
  }
  validation {
    condition     = var.base_name == "" || length(var.base_name) >= 3
    error_message = "Too short: a minimal of 3 characters allowed."
  }
  validation {
    condition     = var.base_name == "" || can(regex("^[a-z0-9][a-z0-9-]+[a-z0-9]$", var.base_name))
    error_message = "May only contain a-z, 0-9, '-' and can't start or end with '-'."
  }
}

variable "base_domain_name" {
  description = "A base domain name to be used for your services such as 'example.org' or 'example.co.uk'."
  type        = string
  default     = ""
  validation {
    condition     = var.base_domain_name == "" || can(regex("^(?:[a-zA-Z0-9\\-]{1,63}\\.)+[a-zA-Z]{2,}$", var.base_domain_name))
    error_message = "Provide a valid domain name such as 'example.org' or 'example.co.uk'."
  }
}

variable "extra_domains" {
  description = "Extra domains for DNS and TLS certificate management (comma seperated)"
  type        = string
  default     = ""
  validation {
    condition     = var.extra_domains == "" || can(regex("^(?:[a-z0-9\\-\\.])+(?:,[a-z0-9\\-\\.]+)*$", var.extra_domains))
    error_message = "Provide comma seperated valid domain names such as 'example.org,example.co.uk'."
  }
}


# === DigitalOcean ===

variable "digitalocean_api_token" {
  description = "Your DigitalOcean API token."
  type        = string
}

variable "digitalocean_region" {
  description = "Which datacenter region to deploy your cluster to."
  type        = string
}

variable "kubernetes_version_prefix" {
  description = "Which Kubernetes minor version to deploy."
  type        = string
  default     = "1"
  validation {
    condition     = can(regex("^\\d+(\\.\\d+)*$", var.kubernetes_version_prefix))
    error_message = "Only minor version notation is allowed."
  }
}

variable "kubernetes_node_size" {
  description = "Which node size to use for your cluster."
  type        = string
  default     = "s-2vcpu-4gb"
}

variable "kubernetes_autoscale" {
  description = "Wether to enable autoscaling on your cluster."
  type        = bool
  default     = true
}

variable "kubernetes_node_autoscale_min" {
  description = "The minium cluster node count."
  type        = number
  default     = 1
}

variable "kubernetes_node_autoscale_max" {
  description = "The maximum cluster node count."
  type        = number
  default     = 5
}


# === GitLab ===

variable "gitlab_namespace_url" {
  description = "The URL the GitLab credentials are scoped to."
  type        = string
  default     = ""
  validation {
    condition     = var.gitlab_namespace_url == "" || can(regex("^https://.+$", var.gitlab_namespace_url))
    error_message = "Only https URLs are allowed."
  }
}

variable "gitlab_api_username" {
  description = "A GitLab deploy token name or your GitLab username with API scope."
  type        = string
  default     = ""
}

variable "gitlab_api_token" {
  description = "A GitLab deploy token or a GitLab personal access token with API scope."
  sensitive   = true
  type        = string
  default     = ""
}


# === Argo CD ===

variable "argo_cd_admin_password" {
  description = "A new Argo CD admin password."
  sensitive   = true
  type        = string
  default     = ""
}

variable "argo_cd_root_app_extra_apps" {
  description = "Additional Argo CD apps to pass to the root app."
  sensitive   = true
  type        = string
  default     = ""
  validation {
    condition     = var.argo_cd_root_app_extra_apps == "" || can(tolist(yamldecode(var.argo_cd_root_app_extra_apps)))
    error_message = "Should be a string as YAML formatted sequence (list)."
  }
}


# Sealed Secrets

variable "sealed_secrets_keypair" {
  description = "A Sealed Secrets key pair to use for the Sealed Secret operator in JSON format containing 'crt' and 'key'"
  type        = string
  default     = ""
  sensitive   = true
  validation {
    condition     = var.sealed_secrets_keypair == "" || can(regex("^-----BEGIN\\sCERTIFICATE-----$", chomp(element(coalescelist(split("\n", trimspace(jsondecode(var.sealed_secrets_keypair == "" ? "{\"crt\":\"\"}" : var.sealed_secrets_keypair).crt))), 0))))
    error_message = "Must be in JSON format with a 'crt' key starting with '-----BEGIN CERTIFICATE-----'."
  }
  validation {
    condition     = var.sealed_secrets_keypair == "" || can(regex("^-----END\\sCERTIFICATE-----$", chomp(element(reverse(coalescelist(split("\n", trimspace(jsondecode(var.sealed_secrets_keypair == "" ? "{\"crt\":\"\"}" : var.sealed_secrets_keypair).crt)))), 0))))
    error_message = "Must be in JSON format with a 'crt' key ending with '-----END CERTIFICATE-----'."
  }
  validation {
    condition     = var.sealed_secrets_keypair == "" || can(regex("^-----BEGIN\\s(RSA\\s)?PRIVATE\\sKEY-----$", chomp(element(coalescelist(split("\n", trimspace(jsondecode(var.sealed_secrets_keypair == "" ? "{\"key\":\"\"}" : var.sealed_secrets_keypair).key))), 0))))
    error_message = "Must be in JSON format with a 'key' key starting with '-----BEGIN PRIVATE KEY-----' or '-----BEGIN RSA PRIVATE KEY-----'."
  }
  validation {
    condition     = var.sealed_secrets_keypair == "" || can(regex("^-----END\\s(RSA?\\s)?PRIVATE\\sKEY-----$", chomp(element(reverse(coalescelist(split("\n", trimspace(jsondecode(var.sealed_secrets_keypair == "" ? "{\"key\":\"\"}" : var.sealed_secrets_keypair).key)))), 0))))
    error_message = "Must be in JSON format with a 'key' key ending with '-----END PRIVATE KEY-----' or '-----END RSA PRIVATE KEY-----'."
  }
}
