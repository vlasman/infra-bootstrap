terraform {
  required_version = ">= 1.3"

  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "2.20.0"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "3.19.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.2.0"
    }
  }
}
