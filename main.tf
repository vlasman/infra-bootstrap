# === General ===

locals {
  base_name = coalesce(var.base_name, random_pet.base_name.id)
}

# https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/pet
resource "random_pet" "base_name" {
  length = 2
}


# === DigitalOcean ===

# https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/id
resource "random_id" "kubernetes_cluster_name" {
  byte_length = 4
  prefix      = "${local.base_name}-"
}

# https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/id
resource "random_id" "kubernetes_node_pool_name" {
  byte_length = 4
  prefix      = "${random_id.kubernetes_cluster_name.hex}-pool-"
}

# https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs
provider "digitalocean" {
  token = var.digitalocean_api_token
}

# https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/data-sources/kubernetes_versions
data "digitalocean_kubernetes_versions" "main" {
  version_prefix = "${var.kubernetes_version_prefix}."
}

# https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/kubernetes_cluster
resource "digitalocean_kubernetes_cluster" "main" {
  name    = random_id.kubernetes_cluster_name.hex
  region  = var.digitalocean_region
  version = data.digitalocean_kubernetes_versions.main.latest_version

  maintenance_policy {
    start_time = "04:00"
    day        = "sunday"
  }

  node_pool {
    name       = random_id.kubernetes_node_pool_name.hex
    size       = var.kubernetes_node_size
    auto_scale = var.kubernetes_autoscale
    min_nodes  = var.kubernetes_node_autoscale_min
    max_nodes  = var.kubernetes_node_autoscale_max
  }
}

locals {
  kubeconfig = yamldecode(digitalocean_kubernetes_cluster.main.kube_config[0].raw_config)
}


# === GitLab ===

# https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs
provider "gitlab" {
  token = var.gitlab_api_token
}

# https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password
resource "random_password" "gitlab_webhook_secret" {
  count = var.gitlab_api_token == "" ? 0 : 1

  length  = 16
  special = false
}

locals {
  gitlab_webhook_secret = length(random_password.gitlab_webhook_secret) > 0 ? random_password.gitlab_webhook_secret[0].result : ""
}

# https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/data-sources/projects
data "gitlab_projects" "projects" {
  count            = var.gitlab_api_token == "" ? 0 : 1
  min_access_level = 40 # Maintainer role (https://docs.gitlab.com/ee/api/members.html)
}

locals {
  all_gitlab_projects         = flatten(data.gitlab_projects.projects[*].projects[*])
  argo_cd_root_app_extra_apps = yamldecode(nonsensitive(var.argo_cd_root_app_extra_apps))
  root_app_repo_urls          = concat([module.argo_cd.root_app_repository_url], local.argo_cd_root_app_extra_apps[*].spec.source.repoURL)
}

# https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_hook
resource "gitlab_project_hook" "argo_cd_root_app_extra_charts" {
  for_each = toset(var.gitlab_api_token == "" ? [] : [for p in local.all_gitlab_projects : tostring(p.id) if contains(local.root_app_repo_urls, p.http_url_to_repo)])

  project     = each.value
  token       = local.gitlab_webhook_secret
  url         = "https://argocd.${var.base_domain_name}/api/webhook"
  push_events = true
}


# === Argo CD ===

# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs
provider "kubernetes" {
  host                   = digitalocean_kubernetes_cluster.main.kube_config[0].host
  cluster_ca_certificate = base64decode(digitalocean_kubernetes_cluster.main.kube_config[0].cluster_ca_certificate)
  token                  = digitalocean_kubernetes_cluster.main.kube_config[0].token
}

# https://gitlab.com/vlasman/terraform-argo-cd/-/blob/initial/docs/include-as-terraform-module.md
module "argo_cd" {
  depends_on = [
    digitalocean_kubernetes_cluster.main,
  ]

  source = "git::https://gitlab.com/vlasman/terraform-argo-cd.git"

  kubernetes_custom_resources_enabled = var.kubernetes_custom_resources_enabled

  kubernetes_namespace = "operations"

  admin_password = var.argo_cd_admin_password
  extra_secrets = {
    "webhook.gitlab.secret" = local.gitlab_webhook_secret
  }

  git_credentials_url           = var.gitlab_namespace_url
  git_credentials_read_username = var.gitlab_api_username
  git_credentials_read_token    = var.gitlab_api_token

  root_app_repository_url = "https://gitlab.com/vlasman/root-app-chart.git"
  root_app_chart_values   = <<-EOT
    baseDomainName: ${var.base_domain_name}

    gitlabApiTokenSecret:
      name: ${kubernetes_secret_v1.gitlab_api.metadata[0].name}
      key: ${keys(kubernetes_secret_v1.gitlab_api.data)[0]}

    # https://argo-cd.readthedocs.io/en/stable/
    argoCd:
      # https://artifacthub.io/packages/helm/argo/argo-cd?modal=values
      values:
        server:
          ingress:
            tls:
            - hosts:
              - "*.{{ $.Values.baseDomainName }}"

    # https://cert-manager.io/docs/
    certManager:
      defaultClusterIssuer:
        enabled: true
        spec:
          acme:
            server: https://acme-v02.api.letsencrypt.org/directory
            privateKeySecretRef:
              name: default-cluster-issuer-account-key
            solvers:
            - http01:
                ingress:
                  serviceType: ClusterIP
            - selector:
                dnsZones:
                - '{{ slice ($.Values.baseDomainName | splitList "." | reverse) 0 2 | reverse | join "." }}'%{for domain in split(",", var.extra_domains)}
                - ${domain}%{endfor}
              dns01:
                # https://cert-manager.io/docs/configuration/acme/dns01/digitalocean/
                digitalocean:
                  tokenSecretRef:
                    name: ${kubernetes_secret_v1.digitalocean_api.metadata[0].name}
                    key: ${keys(kubernetes_secret_v1.digitalocean_api.data)[0]}
      # https://artifacthub.io/packages/helm/cert-manager/cert-manager?modal=values
      values: {}

    sealedSecrets:
      enabled: ${var.sealed_secrets_keypair != ""}

    # https://kubernetes-sigs.github.io/external-dns/
    externalDns:
      providers:
      - name: digitalocean
        # https://artifacthub.io/packages/helm/external-dns/external-dns?modal=values
        txtOwnerId: "${var.base_name}"
        values: |
          {{- if or $.Values.baseDomainName (ne "${var.extra_domains}" "") -}}
          # https://kubernetes-sigs.github.io/external-dns/v0.12.0/tutorials/digitalocean/
          provider: digitalocean
          domainFilters:
          - {{ slice ($.Values.baseDomainName | splitList "." | reverse) 0 2 | reverse | join "." }}%{for domain in split(",", var.extra_domains)}
          - ${domain}%{endfor}
          env:
          - name: DO_TOKEN
            valueFrom:
              secretKeyRef:
                name: ${kubernetes_secret_v1.digitalocean_api.metadata[0].name}
                key: ${keys(kubernetes_secret_v1.digitalocean_api.data)[0]}
          {{- end }}

    %{if var.argo_cd_root_app_extra_apps != ""}
    extraApps:
    ${var.argo_cd_root_app_extra_apps}
    %{endif}
  EOT
}


# === Sealed Secrets ===

# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret_v1
resource "kubernetes_secret_v1" "sealed_secrets_keypair" {
  count = var.sealed_secrets_keypair == "" ? 0 : 1

  metadata {
    name      = "sealed-secrets-keypair"
    namespace = module.argo_cd.kubernetes_namespace
    labels = {
      "app.kubernetes.io/managed-by"                 = "Terraform"
      "sealedsecrets.bitnami.com/sealed-secrets-key" = "active"
    }
  }

  type = "kubernetes.io/tls"

  data = {
    "tls.crt" = var.sealed_secrets_keypair == "" ? "" : jsondecode(var.sealed_secrets_keypair).crt
    "tls.key" = var.sealed_secrets_keypair == "" ? "" : jsondecode(var.sealed_secrets_keypair).key
  }
}


# === Extra secrets ===

# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret_v1
resource "kubernetes_secret_v1" "digitalocean_api" {
  metadata {
    name      = "digitalocean-api-credentials"
    namespace = module.argo_cd.kubernetes_namespace
    labels = {
      "app.kubernetes.io/managed-by" = "Terraform"
    }
  }

  data = {
    "access-token" = var.digitalocean_api_token
  }
}

# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret_v1
resource "kubernetes_secret_v1" "gitlab_api" {
  metadata {
    name      = "gitlab-api-credentials"
    namespace = module.argo_cd.kubernetes_namespace
    labels = {
      "app.kubernetes.io/managed-by" = "Terraform"
    }
  }

  data = {
    "access-token" = var.gitlab_api_token
  }
}
