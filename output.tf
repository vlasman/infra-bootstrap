# === General ===

output "base_name" {
  value = local.base_name
}

output "kubernetes_crds_available" {
  description = "Wether Kubernetes and its Custom Resource Definitions are available"
  value       = digitalocean_kubernetes_cluster.main.version != null && module.argo_cd.kubernetes_crds_available
}

output "install_completed" {
  description = "Wether the Terraform module install is complete"
  value       = module.argo_cd.install_completed
}


# === DigitalOcean ===

output "kubernetes_cluster_name" {
  value = digitalocean_kubernetes_cluster.main.name
}

output "kubernetes_version" {
  value = digitalocean_kubernetes_cluster.main.version
}

output "kubernetes_api_url" {
  value = digitalocean_kubernetes_cluster.main.endpoint
}

output "kubernetes_ca_certificate" {
  value = base64decode(nonsensitive(digitalocean_kubernetes_cluster.main.kube_config[0].cluster_ca_certificate))
}

output "kubernetes_token" {
  sensitive = true
  value     = digitalocean_kubernetes_cluster.main.kube_config[0].token
}

output "kubeconfig" {
  sensitive = true
  value = replace(
    yamlencode(
      merge(
        local.kubeconfig,
        {
          "current-context" = digitalocean_kubernetes_cluster.main.name
        },
        {
          contexts = [
            merge(
              local.kubeconfig.contexts[0],
              {
                name = digitalocean_kubernetes_cluster.main.name
              }
            )
          ]
        }
      )
  ), "\"", "")
}

output "kubeconfig_redacted" {
  value = nonsensitive(
    replace(
      yamlencode(
        merge(
          local.kubeconfig,
          {
            "current-context" = digitalocean_kubernetes_cluster.main.name
          },
          {
            contexts = [
              merge(
                local.kubeconfig.contexts[0],
                {
                  name = digitalocean_kubernetes_cluster.main.name
                }
              )
            ]
          },
          {
            users = [
              merge(
                local.kubeconfig.users[0],
                {
                  user = {
                    token = "___DIGITALOCEAN_API_TOKEN___"
                  }
                }
              )
            ]
          }
        )
    ), "\"", "")
  )
}


# === GitLab ===

output "gitlab_webhook_secret" {
  sensitive = true
  value     = local.gitlab_webhook_secret
}


# === Argo CD ===

output "argo_cd_admin_password" {
  description = "The provided/generated Argo CD admin password."
  sensitive   = true
  value       = module.argo_cd.admin_password
}
