# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/digitalocean/digitalocean" {
  version     = "2.20.0"
  constraints = "2.20.0"
  hashes = [
    "h1:3lPDMpAD5A67iDLhJ9LgVO1oE0dGoJ/e5/UQsu7Lq3c=",
    "h1:8g5nuT0ExA58+OUer5LPKkDYJW3BET5NbgPeI9xYNsg=",
    "h1:Eoa7bO+nTf2LYYDR5QKhUmSuqbby+KpPk20sC2cOf5A=",
    "zh:02f153484a290181da6a38e1cd18112fae2765a653329dfd5defc75aa922fd2b",
    "zh:14d47206855f6e0e80905f9d00e3cddbf3c4cba0439443809fd3a0de5b54e50d",
    "zh:1b721671d41f8f933edb08daa5ef8a9882d313e4c2a66e3cab4dae499789adc5",
    "zh:47b7a160c440afe295d427de3e5f192f68be0cd09c9652cf8407d8c5be3548e4",
    "zh:64986e7784d2586d2e1153113687c59c136c30ad1edb1cf3fc58682e31880325",
    "zh:7b304504dc7ef539d96417ead22135c2a14f7e5d327c984f9fb93bde3ba735ec",
    "zh:8249573279d77977e85dcfb915f6ddb8123c5174abd1f6655e26975134205005",
    "zh:a21aa89e532a31c372219b13cdbacf3629697374829063b14ac3b90063fb2260",
    "zh:ac46c53e8ab625e18617c03c4c7b4136b6f95d6ca6ed3ee6792e0ec986600e23",
    "zh:b41558abb56f6fdf618a415d6c20274f3296a52897466ac9d2510e4eb2aab97a",
    "zh:b9e16e2bc3e60d375a24da64a016855ea95efff1bb81aee8c628cd516db0a315",
    "zh:bbf9d6104e3805743604a2099dcade5d664cf05c309eafe302179e98ca7ca0a7",
    "zh:be931134b07ceb06c11c48ecff550865273990663e5f0614088f4e19f935b50e",
    "zh:bfeded25c6c28932fe9811b1547379112e8768e11d3f03f0a547ff2414f51e15",
    "zh:d5476010ee1527c10875e0bfe7b547ffe1d2ee3970d5a7141cf7b36c8763e5e0",
    "zh:f4d808cfed8de7ff17cc9c09f5850141e0f334b28ee46e19b68dec38c7762a80",
  ]
}

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "3.19.0"
  constraints = "3.19.0"
  hashes = [
    "h1:/raFK5IYh+gMF4na98UECoCKI6OuCDqHFfRNY3XW9fk=",
    "h1:bAvPUfPDgvu7ADQq2RK8Na23wS5gxxsrZ3AR4HQ/p2k=",
    "h1:yAxi+4kQbEaxYqWrSFOnFmTF8BxeZDYk/RgIMmXhXxA=",
    "zh:015276be97812208dbcac65d36af4b9408117d962682088be8ebca34197d9135",
    "zh:06539b3e8a3d8122ea8086dc46495658caf827e5174802f7345764dedc4f19d8",
    "zh:067344cb1a1aaff2cb44a35f6520c04b7f0d48a1a278f84b0ab12078da7c1274",
    "zh:0d9e97f081f898a761d616b7285a93861dc611fac078bfdac84d2a7e146c9341",
    "zh:1c3e89cf19118fc07d7b04257251fc9897e722c16e0a0df7b07fcd261f8c12e7",
    "zh:6fb81e8c53278d8726c15f348507dce8203868e3ab12c10667c21a55a2fa8363",
    "zh:713b2bf9089cce817e58b48c2644fda7e36e7c2ecfabc83b7e5ff4c8a11191b6",
    "zh:878aced23aba1a85efb00c34efe0fa320defac1f77c82be52a8fcb2bee31bf58",
    "zh:96524031be167f3e70d558d00a4e0330746d6f674e50c049eec23637502242a2",
    "zh:a0941413933fbc6ed1e008fd4d71f3fcd6d37aa0d2470a10375c6933868ce498",
    "zh:a311994b403e53bcb2767631058990a8f7a4e5e7b6e88b11bd551fcbd07d9d20",
    "zh:bf23415f2c49253414462dac994b1374717ac1732f4b9ca9bd24609b80812371",
    "zh:c167e0d3d6d2ef70b18967bf36682441e6c9c3ad5af896f25b554aae77645383",
    "zh:eb83fc1084279f39c60946bc7e25c3e1f52db793f87833d84d53944ae33914a9",
    "zh:f067397bd731dcc85f72c7b7a86df11269844d4c88b526f1e6c3708881220a5a",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version     = "2.16.0"
  constraints = "2.16.0"
  hashes = [
    "h1:GcpVjl1LbyGDGGaR0KDJrdVaTKW2ge9g51Ej4yrai6Q=",
    "h1:oo9HgPpYIbxhK9L/a+opbQ0BjVZqid+3h5gr0bchhDs=",
    "h1:zkcz+e1K0SZljjWDqQ4Cytt/kwp1D6og3m9dqHYp+/Q=",
    "zh:172830e270e49b3d6c975383f6c2f1683524ab667e48a481285d535392f29cf4",
    "zh:1b2919c66f6bf49a24adb3f0663e198383562829bc1c06c680cf0a2019571d4f",
    "zh:2c0b1c6032358c11539d1f99ddd803dc37b06127e8d220e9b9a81a233a290a58",
    "zh:2c6b49d0014a4398e35d05ce2303d10482c91b49320555e2389a8b85f28117ea",
    "zh:497e76411feb3f79b8eaa3bb29a387c6d89b888f7d9d028142dc5590ff149e45",
    "zh:771428ba9ed855743fd7e6b7ee7d3d837e401c787da618a8cff5f6e7375a6245",
    "zh:cb15f6d7eaa6aa385215f6d77dcfd5615e40d170800ce9fbee3d73b5c6ad379f",
    "zh:e8de8530e27903d4581b4494a267ab84ab3faeaaa598986fea74a99cfa3b37dc",
    "zh:efd5d1b02d3b68d0b8913372421d292766ba572e54b60b16bc38b439b9865095",
    "zh:f4568bda22c959dc510f9fb8c1ac141ded7c99df4ba430efcd470b13776ce9cb",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
    "zh:fa08fa52d3b4f93d24373a34360855787971532a1f5fe085a4549b04ebf329cc",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version     = "3.2.0"
  constraints = "3.2.0"
  hashes = [
    "h1:IHB49Mm7q/npgP6opLob2HR2AfdTu0fySYLAfrNNO38=",
    "h1:YD109FT06s3sFdAnpsTL/cpcSDcI4NPiWvyF89ilACc=",
    "h1:eeUh6cJ6wKLLuo4q9uQ0CA1Zvfqya4Wn1LecLCN8KKs=",
    "zh:2960977ce9a7d6a7d3e934e75ec5814735626f95c186ad95a9102344a1a38ac1",
    "zh:2fd012abfabe7076f3f2f402eeef4970e20574d20ffec57c162b02b6e848c32f",
    "zh:4cd3234671cf01c913023418b227eb78b0659f2cd2e0b387be1f0bb607d29889",
    "zh:52e695b4fa3fae735ffc901edff8183745f980923510a744db7616e8f10dc499",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:848b4a294e5ba15192ee4bfd199c07f60a437d7572efcd2d89db036e1ebc0e6e",
    "zh:9d49aa432a05748a9527e95448cebee1238c87c97c7e8dec694bfd709683f9c7",
    "zh:b4ad4cf289d3f7408649b74b8639918833613f2a1f3cf51b51f4b2fdaa412dd2",
    "zh:c1544c4b416096fb8d8dbf84c4488584a2844a30dd533b957e9e9e60a165f24e",
    "zh:dc737d6b4591cad8c9a1d0b347e587e846d8d901789b29b4dd401b6cdf82c017",
    "zh:f5645fd39f749dbbf847cbdc87ba0dbd141143f12917a6a8904faf8a9b64111e",
    "zh:fdedf610e0d020878a8f1fedda8105e0c33a7e23c4792fca54460685552de308",
  ]
}
