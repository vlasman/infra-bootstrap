# Infra Bootstrap

This [Terraform](https://www.terraform.io) module allows you to create your base infrastructure by using 'Infrastructure as Code' principles and tools.

Supported cloud providers:

- [DigitalOcean](https://www.digitalocean.com)

## GitLab CI provisioning

1. Fork this repository.

1. Add CI variables.

    | Name                     | Type   | Comments                                                               |
    | ------------------------ | ------ | ---------------------------------------------------------------------- |
    | `DIGITALOCEAN_API_TOKEN` | string | Create it [in the portal][do-tokens]                                   |
    | `DIGITALOCEAN_REGION`    | string | `nyc1`, `ams3`, ... Check [availability the documentation][do-regions] |

    [do-tokens]: https://cloud.digitalocean.com/account/api/tokens
    [do-regions]: https://docs.digitalocean.com/products/platform/availability-matrix/

1. Create a pipeline.

1. Trigger the manual 'apply production' job if the plan job is finished.

1. Download the `kubeconfig.yaml` from the artifacts.

1. Replace `___DIGITALOCEAN_API_TOKEN___` with your DigitalOcean API token.

1. Set your Kubernetes config: `export KUBECONFIG=./kubeconfig.yaml`

1. Test your access to the cluster and if Argo CD is installed.

    ```sh
    kubectl get pods --all-namespaces
    ```

    You should see a list of pods including ones from Argo CD.

    ```sh
    kubectl get apps --all-namespaces
    ```

    You should see the 'root' Argo CD application.

## Local provisioning

1. Clone/fork this repository.

    ```sh
    git clone https://gitlab.com/vlasman/infra-bootstrap.git
    ```

1. Ensure the required prerequisites are installed.

    | executable  | download link                                   |
    | ----------- | ----------------------------------------------- |
    | `terraform` | https://www.terraform.io/downloads              |
    | `kubectl`   | https://kubernetes.io/docs/tasks/tools/#kubectl |

1. Configure the remote GitLab [Terraform HTTP backend](https://www.terraform.io/language/settings/backends/http) (optional).

    ```sh
    cp backend_override.tf.example backend_override.tf
    ```

    Enable and fill in the commented fields in `backend_override.tf`.

1. Initialize Terraform.

    ```sh
    terraform init
    ```

1. Provision the infrastructure.

    The Kubernetes Custom Resource Definitions are initially not available so first we need to create a plan without the CRD-dependend resources. In the second apply they should be available and all resources can be applied.

    ```sh
    # Only needed for the initial apply
    terraform apply -var=kubernetes_custom_resources_enabled=false
    ```

    ```sh
    terraform apply
    ```

    Wait for the provisioning to be complete.

1. Fetch and save the Kubernetes config.

    ```sh
    mkdir -p ~/.kube
    terraform output -raw=true kubeconfig > ~/.kube/config.infra-bootstrap.yaml
    export KUBECONFIG="${HOME}/.kube/config.infra-bootstrap.yaml"
    ```

1. Test your access to the cluster and if Argo CD is installed.

    ```sh
    kubectl get pods --all-namespaces
    ```

    You should see a list of pods including ones from Argo CD.

    ```sh
    kubectl get apps --all-namespaces
    ```

    You should see the 'root' Argo CD application.

1. Navigate to https://argocd.yourdomain.tld to see the Argo CD GUI.

    Use `admin` as username and fetch the password from the Terraform output.

    ```sh
    terraform output -json | jq -er '.argo_cd_admin_password.value'
    ```

## Sealed Secrets

To enable Sealed Secrets, create a `TF_VAR_sealed_secret_keypair` in the CI variables.

```sh
openssl req -x509 -nodes -newkey rsa:4096 -keyout keypair.key -out keypair.crt -subj "/CN=sealed-secret/O=sealed-secret"
jq -n --rawfile crt keypair.crt --rawfile key keypair.key '.crt=$crt | .key=$key' > sealed-secrets-keypair.json
rm keypair.crt keypair.key
```

## Troubleshooting

- _Terraform apply failed with REST client message_

    ```
    ╷
    │ Error: Failed to construct REST client
    │
    │   with module.argo_cd.kubernetes_manifest.root_app[0],
    │   on terraform-argo-cd/main.tf line x, in resource "kubernetes_manifest" "root_app":
    │  x: resource "kubernetes_manifest" "root_app" {
    │
    │ cannot create REST client: no client config
    ```

    The Kubernetes Custom Resource Definitions are initially not available so first we need to create a plan without the CRD-dependend resources. In the second apply they should be available and all resources can be applied.

    ```sh
    # Only needed for the initial apply
    terraform apply -var=kubernetes_custom_resources_enabled=false
    ```

    ```sh
    terraform apply
    ```
